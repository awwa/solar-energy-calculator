# Solar Energy Calculator
Die Tabelle berechnet die elektrische Leistung einer Photovoltaikanlage abhängig von Tageszeit, Jahreszeit, Breitengrad und Eigenschaften der Anlage.

Die Formeln zu den Solaren Winkeln sind aus https://solarsena.com/solar-elevation-angle-altitude/ entnommen

Lizenz ist die GPL2

## Benutzung
- Eingabe nur in den grünen Feldern
- Zwischenergebnisse sind in Fett
- Endergebnisse sind blau
- Die effektive Leistung ist eine Formel und wird sofort neu berechnet
- Für die Tagessumme muss das Makro per Klick auf den Button gestartet werden
- Die Regeln am Ende der Tabelle können für individuelle Abschattungen etc. verwendet werden
- Auf dem „Regel Parkplatz“ ist ein Beispiel für eine Abschattung im Osten
- Das Makro rechnet nur von 4 bis 22 Uhr MEZ (dadurch fehlerhaft in höheren Breiten)
- Keine Berücksichtigung atmosphärischer Effekte

## Eingabefelder

### Kalendertag im jahr
Von 1 bis 365. Sommersonnenwende ist ca. Tag 170

### Latitude / Breite
Die geographische Breite. Süddeutschland ist etwa 48 Grad.

### Uhrzeit - dezimal
Halb elf ist zum Beispiel 10,5

### Nominale Panelleistung
Das was im Verkaufsprospekt steht.

### Panelwinkel
Der vertikale Panelwinkel ist die Ausrichtung zum Himmel.
0 Grad = Panel blickt nach oben
90 Grad = senkrechte Anordnung des Panels

### Panelazimut
Die Himmelsrichtung der Ausrichtung. Im Gegensatz zum Solarwinkel (ist ein Zwischenergebnis) ist hier Norden = 0 Grad. Süden ist demnach 180 Grad.

