
"""
Berechnet die Jahressummen des solaren Energie Ertrags unter verschiedenen Bedingungen
"""

import math, csv

def solarAngleByHour(TimeOfDay24h:int):
    SolarAngleDegree = 15*(TimeOfDay24h - 12)
    # SolarAngle = math.radians(SolarAngleDegree)
    return SolarAngleDegree

def solarDeclination(KalenderTag:int):
    """
    Deklination ist die Neigung der Erdachse zur Sonne
    """
    OffsetSolsticeToNewYear = 10
    DegreePerDay = 360 / 365
    SolarDay = KalenderTag + OffsetSolsticeToNewYear
    DeklinationDegree = -23.44 * math.cos(DegreePerDay * SolarDay)

    # Deklination = math.radians(DeklinationDegree)
    return DeklinationDegree

def solarElevation(Latitude:float, Declination:float, SolarAngle:float):
    LatitudeRad = math.radians(Latitude)
    DeclinationRad = math.radians(Declination)
    SolarAngleRad = math.radians(SolarAngle)
    elevation = math.degrees(math.asin(math.sin(LatitudeRad) * math.sin(DeclinationRad) + math.cos(LatitudeRad) * math.cos(DeclinationRad) * math.cos(SolarAngleRad) ) )
    return elevation

def solarElevationFromDay(Latitude:float, DayOfYear:int, SolarAngle:float):
    declination = solarDeclination(DayOfYear)
    elevation = solarElevation(Latitude, declination, SolarAngle)
    return elevation

def irradiationPanelV(solarElevation, panelAngleV):
    """
    maximum irradiation is 90 degree
    """
    irradiationFactorV = 0
    if solarElevation < 0:
        return 0

    irradiationAngleV = solarElevation + panelAngleV
    irradiationAngleVrad = math.radians(irradiationAngleV)
    irradiationFactorV = math.sin(irradiationAngleVrad)
    if irradiationFactorV > 0:
        return irradiationFactorV
    else:
        return 0

def irradiationPanelh(solarAngle, panelAzimut):
    """
    maximum irradiation is at 90 degree
    solarAngle: 0 is south
    PanelAzimut: 0 is north
    """
    irradiationAngleHdeg = solarAngle + 180 - panelAzimut + 90
    irradiationAngleHrad = math.radians(irradiationAngleHdeg)
    factorH = math.sin(irradiationAngleHrad)
    if factorH < 0:
        return 0
    else:
        return factorH

def irradiationPanel(panelAzimut, solarAngle, solarElevation, panelAngleV):
    """
    maximum irradiation is at 90 degree
    solarAngle: 0 is south
    PanelAzimut: 0 is north
    """
    h = irradiationPanelh(solarAngle=solarAngle, panelAzimut=panelAzimut)
    v = irradiationPanelV(solarElevation=solarElevation, panelAngleV=panelAngleV)
    return h*v

def panelPowerFactor(latitudeDegree, calendarDay, TimeOfDay24h, panelAzimut, panelAngleV):
    solarAngle = solarAngleByHour(TimeOfDay24h)
    solarElevation = solarElevationFromDay(latitudeDegree, calendarDay, solarAngle)
    irradiationFactor = irradiationPanel(panelAzimut, solarAngle, solarElevation, panelAngleV)
    return irradiationFactor


def main():
    calendarDay = 222
    latitudeDegree = 48
    for hour in range(23):
        panelAzimut = 190
        panelAngleV = 90
        solarAngle = solarAngleByHour(hour)
        # print ("%s" % solarElevationFromDay(LatitudeDegree, CalendarDay, solarAngle))
        print ("%s" % panelPowerFactor(latitudeDegree, calendarDay, hour, panelAzimut, panelAngleV))


if __name__ == "__main__":
    main()